import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserListComponent } from './pages/user-list/user-list.component';
import { RegisterComponent } from './pages/register/register.component';

const routes: Routes = [
    { path: 'users', component: UserListComponent },
    { path: 'register', component: RegisterComponent },
    { path: '**', redirectTo: 'users' },
];
@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
