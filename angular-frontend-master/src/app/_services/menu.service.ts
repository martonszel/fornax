import { Injectable } from '@angular/core';
// import { MenuItem } from '@model/menu-item';
import {MenuItem} from 'primeng/api';
import { Observable, of } from 'rxjs';
@Injectable({
    providedIn: 'root'
})
export class MenuService {

constructor() { }

getMenuList(): Observable<MenuItem[]> {
    return of([
        { id: '1', name: 'Felhasználók', icon: 'users', link: 'users', label: 'Felhasználók', url: '/users' },
        { id: '2', name: 'Register Users', icon: 'user-plus', link: 'register', label: 'Register Users',  url: '/register'}
    ]);
    }
}
