import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from '@model/user';
import { Observable, of } from 'rxjs';
import {MessageService} from 'primeng/api';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private userList: User[] = [
    {
      id: 1,
      name: 'Pisti',
      email: 'semmi@semmi.hu',
      address: 'Teszt cím',
      birthdate: new Date()
    }
  ];

  constructor(private messageService: MessageService) {}

    getUsers(): Observable<User[]> {
    return of(JSON.parse(localStorage.getItem('userlist')));
        }

    addUser(data) {
        const storage = JSON.parse(localStorage.getItem('userlist'));
        storage.push({...data, id: '_' + Math.random().toString(36).substr(2, 9), birthdate: new Date(data.birthDate)});
        localStorage.setItem('userlist', JSON.stringify(storage));
        this.messageService.add({severity: 'success', summary: 'Success Message', detail: 'User Added'});
        }
    deleteUser() {
        const storage = JSON.parse(localStorage.getItem('userlist'));
        storage.pop();
        localStorage.setItem('userlist', JSON.stringify(storage));
        location.reload();
        }
    clear() {
        this.messageService.clear();
        }
}
    /*
    addUser(data) {
        this.getUsers().subscribe( ul => {
        ul.push({...data, id: '_' + Math.random().toString(36).substr(2, 9), birthdate: new Date(data.birthDate) });
        localStorage.setItem('userlist', JSON.stringify(ul));
        });}*/
