export class MenuItem {
    id: number;
    name: string;
    icon: string;
    link: string;
    label: string;
    url: string;
}
