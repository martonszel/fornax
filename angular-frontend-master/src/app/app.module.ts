import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { far } from '@fortawesome/free-regular-svg-icons';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { TableModule } from 'primeng/table';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {ButtonModule} from 'primeng/button';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './layout/header/header.component';
import { MenuItemComponent } from './layout/menu/menu-item/menu-item.component';
import { MenuComponent } from './layout/menu/menu.component';
import { UserListComponent } from './pages/user-list/user-list.component';
import { RegisterComponent } from './pages/register/register.component';
import {InputTextModule} from 'primeng/inputtext';
import { UserService } from '@service/user.service';
import {MenuModule} from 'primeng/menu';
import {TieredMenuModule} from 'primeng/tieredmenu';
import {MessagesModule} from 'primeng/messages';
import {MessageModule} from 'primeng/message';
import {MessageService} from 'primeng/api';

@NgModule({
    declarations: [
        AppComponent,
        HeaderComponent,
        MenuComponent,
        MenuItemComponent,
        UserListComponent,
        RegisterComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        FontAwesomeModule,
        TableModule,
        FormsModule,
        ReactiveFormsModule,
        ButtonModule,
        InputTextModule,
        MenuModule,
        BrowserAnimationsModule,
        TieredMenuModule,
        MessagesModule,
        MessageModule
    ],
    providers: [UserService, MessageService],
    bootstrap: [AppComponent]
})
export class AppModule {
    constructor() {
        // FontAwesome
        library.add(fas, far);
    }

}
