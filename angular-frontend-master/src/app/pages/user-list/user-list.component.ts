import { Component, OnInit } from '@angular/core';
import { User } from '@model/user';
import { UserService } from '@service/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {
  public users: User[];

  clonedUsers: { [s: string]: User; } = {};

  constructor(private userService: UserService, private router: Router) {}

  ngOnInit() {
    this.reload();
    }

  reload() {
    this.userService.getUsers().subscribe(res => (this.users = res));
    }
  gotoRegister() {
    this.router.navigate(['/register']);
    }

  onRowEditInit(users: User) {
    this.clonedUsers[users.id] = {...users};
    }

  onRowEditSave(users) {
    delete this.clonedUsers[users.id];
    alert('Table is updated');
    }

  onRowEditCancel(users: User, index: number) {
    this.users[index] = this.clonedUsers[users.id];
    delete this.clonedUsers[users.id];
    }
  deleteUser() {
    this.userService.deleteUser();
    }
}
