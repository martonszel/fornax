import { Component, OnInit } from '@angular/core';
import { UserService } from '@service/user.service';
import { User } from '@model/user';
import {MessageService} from 'primeng/api';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  public users: User[];

  constructor(private userService: UserService,
              private messageService: MessageService
    ) { }
  ngOnInit() {
    }
  onClick(userForm) {
    this.userService.addUser(userForm.value);
    }
  clear() {
      this.userService.clear();
  }
}

