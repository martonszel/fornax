import { Component, OnInit } from '@angular/core';
import { MenuService } from '@service/menu.service';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public menuList: MenuItem[];

  constructor(private menuService: MenuService) { }

  ngOnInit() {
    this.menuService.getMenuList().subscribe(res => this.menuList = res);
    }
}
